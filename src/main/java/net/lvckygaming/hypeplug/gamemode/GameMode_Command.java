package net.lvckygaming.hypeplug.gamemode;

import net.lvckygaming.hypeplug.main.Main;
import org.bukkit.GameMode;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

public class GameMode_Command implements CommandExecutor {
    public boolean onCommand(CommandSender sender, Command cmd, String label, String[] args) {
        Player p = (Player) sender;
        if (Main.lock == false) {
            if (p.hasPermission("hp.gamemode")) {
                if (args.length == 1) {
                    if (args[0].equalsIgnoreCase("1")) {
                        p.sendMessage(Main.p + "§6Du bist nun im Creative mode!");
                        p.setGameMode(GameMode.CREATIVE);
                    } else if (args[0].equalsIgnoreCase("2")) {
                        p.sendMessage(Main.p + "§6Du bist nun im Adventure mode!");
                        p.setGameMode(GameMode.ADVENTURE);
                    } else if (args[0].equalsIgnoreCase("3")) {
                        p.sendMessage(Main.p + "§6Du bist nun im SPECTATOR mode!");
                        p.setGameMode(GameMode.SPECTATOR);

                    } else if (args[0].equalsIgnoreCase("0")) {
                        p.sendMessage(Main.p + "§6Du bist nun im Survival mode!");
                        p.setGameMode(GameMode.SURVIVAL);

                    }else {
                        p.sendMessage(Main.p + "§e Mache /gm(gamemode) 0|1|2|3");
                        p.setHealth(0);
                    }
                }else {
                    p.sendMessage(Main.p + "§e Mache /gm(gamemode) 0|1|2|3");
                }
            }else {
                p.sendMessage("§4Die fehlt Berechtigung:§e hp.gamemode");
            }
            } else {
            p.sendMessage(Main.msl);
            }

            return false;
        }
    }

